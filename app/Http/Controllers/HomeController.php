<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Core\Manager\ConnectionManager;
use App\Core\Manager\RedisManager;
use App\Core\Utils\Validator;

class HomeController extends Controller
{
    /**
     * Show the home page.
     *
     * @return View
     */
    public function index(Request $request)
    {
        try {
            $get = $request->query->all();
            $refresh = false;

            if (Validator::validate($get, 'refresh', null, 'null')) {
                $refresh = empty($get['refresh']) ? 1 : $get['refresh'] + 1;
            }   

            // $redis = new RedisManager();

            return view('pages/homepage');

        } catch (\Exception $e) {
            abort(404);
        }
    }

    /**
     * Show the departement page.
     *
     * @return View
     */
    public function departement(Request $request)
    {
        try {
            $get = $request->query->all();
            $refresh = false;

            if (Validator::validate($get, 'refresh', null, 'null')) {
                $refresh = empty($get['refresh']) ? 1 : $get['refresh'] + 1;
            }   

            // $redis = new RedisManager();

            return view('pages/departement');

        } catch (\Exception $e) {
            abort(404);
        }
    }

    /**
     * Show the project page.
     *
     * @return View
     */
    public function project(Request $request)
    {
        try {
            $get = $request->query->all();
            $refresh = false;

            if (Validator::validate($get, 'refresh', null, 'null')) {
                $refresh = empty($get['refresh']) ? 1 : $get['refresh'] + 1;
            }   

            // $redis = new RedisManager();

            return view('pages/project');

        } catch (\Exception $e) {
            abort(404);
        }
    }

    /**
     * Show the test post departement.
     *
     * @return print_r
     */
    public function departementPost(Request $request)
    {
        dd($request->all());
    }

}
