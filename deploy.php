<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'ds-collaboration');

// Project repository
set('repository', 'https://yogadailysoc@bitbucket.org/dailysocial/collaboration-web.git');

set('keep_releases', 5);

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', ['config/app.php','.env','config/database.php','public/robots.txt','clear']);
add('shared_dirs', ['storage']);

// Writable dirs by web server 
//add('writable_dirs', ['storage','bootstrap/cache']);


// Hosts

host('collaboration.dailysocial.id')
	->user('root')
	->configFile('~/.ssh/config')
 	->set('composer_options', 'update --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction')
  	->set('branch', 'master')
    ->set('deploy_path', '/var/www/ds-collaboration');    
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

desc('Execute artisan migrate');
task('artisan:migrate', function () {
    writeln('No Migrate');
})->once();

task('artisan:optimize', function () {
	writeln('No Artisan Optimize');    
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

