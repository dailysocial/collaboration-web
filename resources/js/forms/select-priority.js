$('.select-priority').on('change', function(e) {
    var classButton = "";
    if($(this).children(':selected').val() == "high") {
        classButton = "button--red";
    }else if($(this).children(':selected').val() == "medium"){
        classButton = "button--yellow";
    }else {
        classButton = "button--blue"
    }

    $(this).attr('class', 'select-priority button button--rounded text__roboto-12 text-white w-100').addClass(classButton);
    
});