<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')Collaboration Tools</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}" />
    @yield('stylesheets')


    @yield('topscript')
</head>

<body>
    <div id="wrapper" class="d-flex">
        <aside class="sidebar__menu">
            @include('partials/sidebar')
        </aside>
        <div class="collaboration__content">
            @yield('content')
        </div>
    </div>
    @yield('footer')
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/app.js"></script>
    <script type="text/javascript" src="/js/library.js"></script>
    @yield('bottomscript')
</body>

</html>