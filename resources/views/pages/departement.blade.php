@extends('base/base')
@section('title', 'Overview | ')

@section('content')
    <div class="content__wrapper">
        <article class="container-fluid mt-5 mb-5">
            <section class="row align-items-center mb-4">
                <div class="col-md-9 border-right noborder-mobile">
                    <section class="row align-items-center">
                        <div class="col-md-6 d-flex departement__wrapper">
                            <div class="departement__icon">
                                <div class="position-relative">
                                    <span class="text-white font-weight-bold">PR</span>
                                </div>
                            </div>
                            <div class="departement__content pr-3 pl-3 mb-4">
                                <h5 class="mb-1 text__roboto-30 font-weight-bold text-grey">
                                    Product Team
                                    <span class="ml-3">
                                        <a>
                                            <img src="{{ asset('images/icons/edit-icon.png') }}" width="20" />
                                        </a>
                                    </span>
                                    <span>
                                        <a>
                                            <img src="{{ asset('images/icons/setting-icon.png') }}" width="20" />
                                        </a>
                                    </span>
                                </h5>
                                <p class="text__roboto-18 text-grey-light">Private Discussion</p>
                            </div>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <div class="departement__content pr-3 pl-3 mb-4">
                                <div class="content__image clearfix">
                                    <a href="/">
                                        <div class="image__list image--add float-right position-relative ml-2 mt-1">
                                            <span class="text__roboto-20 text-white font-weight-bold">+</span>
                                        </div>
                                    </a>
                                    <div class="image__list image--large float-right position-relative">
                                        <span>4+</span>
                                    </div>
                                    <div class="image__list image--large float-right" style="background-image:url('./images/gerrard.jpeg');"></div>
                                    <div class="image__list image--large float-right" style="background-image:url('./images/gerrard.jpeg');"></div>
                                    <div class="image__list image--large float-right" style="background-image:url('./images/gerrard.jpeg');"></div>
                                    <div class="image__list image--large float-right" style="background-image:url('./images/gerrard.jpeg');"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-3 pr-md-4 pl-md-4">
                    <form class="pb-4">
                        <div class="input-group search__form">
                            <input class="form-control py-2 rounded-pill mr-1 pr-5" type="search" placeholder="search in here" >
                            <span class="input-group-append">
                                <button class="btn rounded-pill border-0 ml-n5" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>                    
                    </form>
                </div>
            </section>
            <section class="row">
                <div class="col-md-9 border-right noborder-mobile">
                    <div class="table-responsive-md">
                        <table class="table table__wrapper">
                            <thead>
                                <tr class="d-flex">
                                    <th class="col-md-4">Task Name</th>
                                    <th class="col-md-2">Assignment</th>
                                    <th class="col-md-2">Due Date</th>
                                    <th class="col-md-2">Priority</th>
                                    <th class="col-md-2">Stage</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="mb-3">
                        <div class="clearfix">
                            <div class="float-left">
                                <p class="font-weight-bold text__roboto-22 text-grey cursor-pointer trigger__title m-0" data-toggle="collapse" data-target="#table1" aria-expanded="false" aria-controls="table1">
                                    Scora App<span class="ml-3 d-inline-block"><i class="fas fa-chevron-down"></i></span>
                                </p>
                            </div>
                            <div class="float-right">
                                <div class="m-0">
                                    <div class="departement__content">
                                        <div class="content__image clearfix">
                                            <div class="image__list image--small float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                                            <span class="ml-2 text__roboto-14 text-grey-light">Irfan Bachdim. <strong>(Lead Project)</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="collapse show table-responsive-md" id="table1">
                        <table class="table table__wrapper table-bordered">
                            <tbody>
                                @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-2">
                                            Yohannes Adhi
                                        </td>
                                        <td class="col-md-2">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--green text__roboto-12 text-white w-100">Reviewing</button>
                                        </td>
                                    </tr>
                                @endfor
                                @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-2">
                                            Yohannes Adhi
                                        </td>
                                        <td class="col-md-2">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--red text__roboto-12 text-white w-100">On Progress</button>
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                    <div class="mb-3">
                        <div class="clearfix">
                            <div class="float-left">
                                <p class="font-weight-bold text__roboto-22 text-grey cursor-pointer trigger__title m-0" data-toggle="collapse" data-target="#table2" aria-expanded="false" aria-controls="table2">
                                    Orchestra Directory<span class="ml-3 d-inline-block"><i class="fas fa-chevron-down"></i></span>
                                </p>
                            </div>
                            <div class="float-right">
                                <div class="m-0">
                                    <div class="departement__content">
                                        <div class="content__image clearfix">
                                            <div class="image__list image--small float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                                            <span class="ml-2 text__roboto-14 text-grey-light">Irfan Bachdim. <strong>(Lead Project)</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="collapse show table-responsive-md" id="table2">
                        <table class="table table__wrapper table-bordered">
                            <tbody>
                            @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-2">
                                            Yohannes Adhi
                                        </td>
                                        <td class="col-md-2">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--green text__roboto-12 text-white w-100">Reviewing</button>
                                        </td>
                                    </tr>
                                @endfor
                                @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-2">
                                            Yohannes Adhi
                                        </td>
                                        <td class="col-md-2">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--red text__roboto-12 text-white w-100">On Progress</button>
                                        </td>
                                    </tr>
                                @endfor
                                @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-2">
                                            Yohannes Adhi
                                        </td>
                                        <td class="col-md-2">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--green text__roboto-12 text-white w-100">Reviewing</button>
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-3 pr-md-4 pl-md-4">
                    <div>
                        <h5 class="text__roboto-22 font-weight-bold text-grey text-center mb-4">Create Task</h5>
                        <form action="/departementPost" method="post">
                            @csrf
                            <div class="form-group-2">
                                <input id="input-title" type="text" class="form-control-2" name="title" required="required"/>
                                <span class="highlight"></span><span class="form-control-2__bar"></span>
                                <label class="form-control-2__label">Title</label>
                            </div>
                            <div class="form-group-2">
                                <input id="input-board-card" type="text" class="form-control-2" name="board-card" required="required"/>
                                <span class="highlight"></span><span class="form-control-2__bar"></span>
                                <label class="form-control-2__label">Board Card</label>
                            </div>
                            <div class="form-group-2">
                                <label class="label-textarea">Description</label>
                                <textarea cols="5" rows="5" class="form-control input-textarea" name="description"></textarea>    
                            </div>
                            <div class="form-group-2">
                                <input id="input-due-date" type="date" class="form-control-2" name="due-date" required="required"/>
                                <span class="highlight"></span><span class="form-control-2__bar"></span>
                                <label class="form-control-2__label"></label>
                            </div>
                            <div class="form-group">
                                <label class="label-textarea" for="input-name">Assign To</label>
                                <input id="input-name" type="text" class="form-control" name="input-name" required="required" />
                            </div>
                            <div class="pt-5 btn-wrapper text-center">
                                <button id="btn-login" type="submit" class="button button--rounded button--white text__roboto-14 text-grey font-weight-bold pl-3 pr-3 pt-2 pb-2">Submit Task</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </article>
    </div>
@endsection

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/bootstrap-tagsinput-custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/bootstrap-tagsinput.css') }}" />
@endsection
@section('bottomscript')
    <script type="text/javascript" src="/js/forms/select-priority.js"></script>
    <script type="text/javascript" src="/assets/js/typeahead.bundle.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap-tagsinput.min.js"></script>


    <script>
        var data ='[{ "id": 1, "name": "Artha", "fullname": "Artha Perkasa" }, { "id": 2, "name": "Dimas", "fullname": "Dimas Perkasa" }, { "id": 3, "name": "Jarot", "fullname": "Jarot Perkasa" }, { "id": 4, "name": "Bagus", "fullname": "Bagus Perkasa" }, { "id": 5, "name": "Benny", "fullname": "Benny Perkasa" }, { "id": 6, "name": "Digal", "fullname": "Digal Perkasa" } ]';

        //get data pass to json
        var task = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: jQuery.parseJSON(data) //your can use json type
        });

        task.initialize();

        var elt = $("#input-name");
        elt.tagsinput({
            itemValue: "id",
            itemText: "name",
            typeaheadjs: {
                name: "add",
                displayKey: "fullname",
                source: task.ttAdapter()
            }
        });
        
    </script>
@endsection