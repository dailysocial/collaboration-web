@extends('base/base')
@section('title', 'Overview | ')

@section('content')
    <div class="content__wrapper d-flex justify-content-center flex-column">
        <article class="container">
            <section class="row mb-5">
                <div class="col-12 text-center">
                    <h1 class="text__roboto-30 text-grey font-weight-bold">Dailysocial Departement</h1>
                    <p class="text__roboto-18 text-grey-light">All project in your departement</p>
                </div>
            </section>
            <section class="row mb-5">
                <div class="col-md-4 d-flex mb-4 departement__wrapper">
                    <div class="departement__icon">
                        <div class="position-relative">
                            <span class="text-white font-weight-bold">PR</span>
                        </div>
                    </div>
                    <div class="departement__content pr-3 pl-3 mb-4">
                        <h5 class="mb-2 text__roboto-22 text-grey font-weight-bold">Product Team</h5>
                        <div class="content__image clearfix">
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left position-relative">
                                <span class="text-grey-thins text__roboto-14">4+</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex mb-4 departement__wrapper">
                    <div class="departement__icon">
                        <div class="position-relative">
                            <span class="text-white font-weight-bold">MK</span>
                        </div>
                    </div>
                    <div class="departement__content pr-3 pl-3">
                        <h5 class="mb-2 text__roboto-22 text-grey font-weight-bold">Marketing Team</h5>
                        <div class="content__image clearfix">
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex mb-4 departement__wrapper">
                    <div class="departement__icon">
                        <div class="position-relative">
                            <span class="text-white font-weight-bold">EV</span>
                        </div>
                    </div>
                    <div class="departement__content pr-3 pl-3">
                        <h5 class="mb-2 text__roboto-22 text-grey font-weight-bold">Event Team</h5>
                        <div class="content__image clearfix">
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex mb-4 departement__wrapper">
                    <div class="departement__icon">
                        <div class="position-relative">
                            <span class="text-white font-weight-bold">ED</span>
                        </div>
                    </div>
                    <div class="departement__content pr-3 pl-3">
                        <h5 class="mb-2 text__roboto-22 text-grey font-weight-bold">Editorial Team</h5>
                        <div class="content__image clearfix">
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                            <div class="image__list float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="row">
                <div class="col-12 text-center">
                    <a class="text__roboto-18 text-blue font-weight-bold">Create New Boards</a><br />
                    <a class="text__roboto-18 text-grey-thin">Delete Boards</a>
                </div>
            </section>
        </article>
    </div>
@endsection