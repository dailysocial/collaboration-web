@extends('base/base')
@section('title', 'Overview | ')

@section('content')
    <div class="content__wrapper">
        <article class="container-fluid mt-5 mb-5">
            <section class="row align-items-center mb-4">
                <div class="col-md-9 border-right noborder-mobile">
                    <section class="row align-items-center">
                        <div class="col-md-6 d-flex departement__wrapper">
                            <div class="departement__icon">
                                <div class="position-relative">
                                    <span class="text-white font-weight-bold">YP</span>
                                </div>
                            </div>
                            <div class="departement__content pr-3 pl-3 mb-4">
                                <h5 class="mb-1 text__roboto-30 font-weight-bold text-grey">
                                    Your Project
                                </h5>
                                <p class="text__roboto-18 text-grey-light">Project Active</p>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-3 pr-md-4 pl-md-4">
                    <form class="pb-4">
                        <div class="input-group search__form">
                            <input class="form-control py-2 rounded-pill mr-1 pr-5" type="search" placeholder="search in here" >
                            <span class="input-group-append">
                                <button class="btn rounded-pill border-0 ml-n5" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>                    
                    </form>
                </div>
            </section>
            <section class="row">
                <div class="col-md-9 border-right noborder-mobile">
                    <div class="table-responsive-md">
                        <table class="table table__wrapper table-bordered">
                            <thead>
                                <tr class="d-flex">
                                    <th class="col-md-4">Task Name</th>
                                    <th class="col-md-4">Due Date</th>
                                    <th class="col-md-2">Priority</th>
                                    <th class="col-md-2">Stage</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="mb-3">
                        <div class="clearfix">
                            <div class="float-left">
                                <p class="font-weight-bold text__roboto-22 text-grey cursor-pointer trigger__title m-0" data-toggle="collapse" data-target="#table1" aria-expanded="false" aria-controls="table1">
                                    Scora App<span class="ml-3 d-inline-block"><i class="fas fa-chevron-down"></i></span>
                                </p>
                            </div>
                            <div class="float-right">
                                <div class="m-0">
                                    <div class="departement__content">
                                        <div class="content__image clearfix">
                                            <div class="image__list image--small float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                                            <span class="ml-2 text__roboto-14 text-grey-light">Irfan Bachdim. <strong>(Lead Project)</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="collapse show table-responsive-md" id="table1">
                        <table class="table table__wrapper table-bordered">
                            <tbody>
                                @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-4">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--green text__roboto-12 text-white w-100">Reviewing</button>
                                        </td>
                                    </tr>
                                @endfor
                                @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-4">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--red text__roboto-12 text-white w-100">On Progress</button>
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                    <div class="mb-3">
                        <div class="clearfix">
                            <div class="float-left">
                                <p class="font-weight-bold text__roboto-22 text-grey cursor-pointer trigger__title m-0" data-toggle="collapse" data-target="#table2" aria-expanded="false" aria-controls="table2">
                                    Orchestra Directory<span class="ml-3 d-inline-block"><i class="fas fa-chevron-down"></i></span>
                                </p>
                            </div>
                            <div class="float-right">
                                <div class="m-0">
                                    <div class="departement__content">
                                        <div class="content__image clearfix">
                                            <div class="image__list image--small float-left" style="background-image:url('./images/gerrard.jpeg');"></div>
                                            <span class="ml-2 text__roboto-14 text-grey-light">Irfan Bachdim. <strong>(Lead Project)</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="collapse show table-responsive-md" id="table2">
                        <table class="table table__wrapper table-bordered">
                            <tbody>
                            @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-4">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--green text__roboto-12 text-white w-100">Reviewing</button>
                                        </td>
                                    </tr>
                                @endfor
                                @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-4">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--red text__roboto-12 text-white w-100">On Progress</button>
                                        </td>
                                    </tr>
                                @endfor
                                @for ($i = 0; $i < 2; $i++)
                                    <tr class="d-flex">
                                        <td class="col-md-4">
                                            Team Discussion
                                        </td>
                                        <td class="col-md-4">
                                            Dec 5 - 10
                                        </td>
                                        <td class="col-md-2">
                                            <form>
                                                <select class="select-priority button button--rounded button--black text__roboto-12 text-white w-100">
                                                    <option>-- priority --</option>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </form>
                                        </td>
                                        <td class="col-md-2">
                                            <button class="button button--rounded button--green text__roboto-12 text-white w-100">Reviewing</button>
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-3 pr-md-4 pl-md-4">
                    <div>
                        <h5 class="text__roboto-22 font-weight-bold text-grey mb-4">Project Overview</h5>
                        <h5 class="text__roboto-22 font-weight-bold text-grey mb-4">Scora App - UI Design</h5>
                        <p class="text__roboto-17 text-grey-light">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora libero repellat, consequuntur consequatur animi aliquid corrupti quam sapiente officia repellendus. Facere inventore velit aliquam, quas laborum consequatur, maiores quaerat modi quidem id et consectetur ullam voluptates voluptatibus fugit placeat! Ipsum vitae corrupti tenetur? Error quae eius mollitia eaque in tenetur.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="text__roboto-12 text-grey-thins font-weight-bold mb-1">Due Date</p>
                            <p class="text__roboto-20 text-yellow m-0">Dec 5, 2019</p>
                        </div>
                        <div class="col-md-6">
                            <p class="text__roboto-12 text-grey-thins font-weight-bold mb-2">Assignments</p>
                            <div class="departement__content">
                                <div class="content__image clearfix">
                                    <div class="image__list image--small d-inline-block" style="background-image:url('./images/gerrard.jpeg');"></div>
                                    <div class="image__list image--small d-inline-block" style="background-image:url('./images/gerrard.jpeg');"></div>
                                    <div class="image__list image--small d-inline-block" style="background-image:url('./images/gerrard.jpeg');"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-md-3">
                            <p class="text__roboto-12 text-grey-thins font-weight-bold mb-1">Link Attachments</p>
                            <a class="text__roboto-20 text-blue">drive.google.com/scoraapp</a>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
@endsection

@section('bottomscript')
    <script type="text/javascript" src="/js/forms/select-priority.js"></script>
@endsection