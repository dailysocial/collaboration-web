<div class="text-center" id="sidebar-wrapper">
    <div class="logo-group"> 
        <a>
            <img src="{{ asset('images/orchestra-logo.png') }}" width="30" />
        </a>
    </div>
    <div class="list-group">
        <a class="mb-4" href="{{ route('HomeView') }}">
            @if(Request::segment(1) == null)
                <img src="{{ asset('images/icons/home-icon-active.png') }}" width="25" />
            @else
                <img src="{{ asset('images/icons/home-icon.png') }}" width="25" />
            @endif
        </a>
        <a class="mb-4" href="{{ route('DepartementView') }}">
            @if(Request::segment(1) == 'departement')
                <img src="{{ asset('images/icons/list-icon-active.png') }}" width="25" />
            @else
                <img src="{{ asset('images/icons/list-icon.png') }}" width="25" />
            @endif
        </a>
        <a class="mb-4" href="{{ route('ProjectView') }}">
            @if(Request::segment(1) == 'project')
                <img src="{{ asset('images/icons/project-icon-active.png') }}" width="25" />
            @else
                <img src="{{ asset('images/icons/project-icon.png') }}" width="25" />
            @endif

        </a>
    </div>
    <div class="profile-group">
        <a class="mb-4 position-relative">
            <img src="{{ asset('images/icons/notification-icon.png') }}" width="25" />
            <span class="notification"></span>
        </a>
        <a class="mb-4">
            <div class="departement__content">
                <div class="content__image">
                    <div class="image__list mx-auto" style="background-image:url('./images/gerrard.jpeg');"></div>
                </div>
            </div>
        </a>
    </div>
</div>