<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcomes', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@index')->name('HomeView');
Route::get('/departement', 'HomeController@departement')->name('DepartementView');
Route::get('/project', 'HomeController@project')->name('ProjectView');
Route::post('/departementPost', 'HomeController@departementPost')->name('DepartementPost');